This program is designed to keep track of the 
orders in the system for a flooring company.
This is made possible through the logic in our
program that operates on data pulled in from
text files. Below is a brief outline of the
project:

*Objects that are Instantiated in controller:
	*Product - from text file and product class
	*Order - from text file and order class
	*Order Book - hashmap of dates to arraylist of orders

*Controller
	*Creates HashMap of all of the dates tied to an array list
	of orders
	*Pulls string arrays from File Access class in order to create the order objects
	*Passes a hashmap to File Access class to print orders to files
