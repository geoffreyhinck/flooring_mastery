/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface orderbookInterface {

    public void addOrder(Order newOrder, String dateIn);

    public ArrayList<Order> findOrderListByDate(String dateIn);

    public Order findOrderByNum(String orderNumIn, ArrayList<Order> wantedList);

    public void removeOrder(String dateIn, String orderNum);

    public String generateOrderNum(String dateIn);

}
