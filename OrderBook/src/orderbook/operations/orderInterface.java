/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

/**
 *
 * @author apprentice
 */
public interface orderInterface {
    public String getOrderNum();
    public void setOrderNum(String orderNum);
    public String getCustomerName();
    public void setCustomerName(String Name);
    public String getState();
    public void setState(String stateIn);
    public double getTaxRate();
    public void setTaxRate(double rate);
    public Product getProductType();
    //public Product setProductType(int i);
    public void setProductType(Product newProduct);
    public double getArea();
    public void setArea(double area);
    public double getMaterialCost();
    public void setMaterialCost();
    public double getLaborCost();
    public void setLaborCost();
    public double getTotalCost();
    public void setTotalCost();
    public double getTax();
    public void setTax();
    
}
