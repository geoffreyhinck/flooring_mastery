/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class OrderBook implements orderbookInterface {

    private HashMap<String, ArrayList<Order>> orderByDate;

    public OrderBook(HashMap<String, ArrayList<Order>> newList) {
        orderByDate = newList;
    }

    @Override
    public void addOrder(Order newOrder, String dateIn) {
        /* might need to go to the controller*/
        /*handle the no pointer*/
        if (orderByDate.containsKey(dateIn)) {
            ArrayList<Order> tempArray = findOrderListByDate(dateIn);

            tempArray.add(newOrder);
        } else {
            ArrayList<Order> newArray = new ArrayList();
            newArray.add(newOrder);
            orderByDate.put(dateIn, newArray);
        }
    }

    @Override
    public ArrayList<Order> findOrderListByDate(String dateIn) {
        Set<String> keySet = this.orderByDate.keySet();
        for (String key : keySet) {
            if (key.equalsIgnoreCase(dateIn)) {
                return this.orderByDate.get(key);
            }
            //handle null pointer
        }
        return null;
    }

    @Override
    public Order findOrderByNum(String orderNumIn, ArrayList<Order> wantedList) {
        Iterator<Order> iter = wantedList.iterator();
        while (iter.hasNext()) {
            Order foundOrder = iter.next();
            if (foundOrder.getOrderNum().equalsIgnoreCase(orderNumIn)) {
                return foundOrder;
            }

        }
        //handle null pointer
        return null;
    }

    @Override
    public void removeOrder(String dateIn, String orderNum) {
        ArrayList<Order> removeList = findOrderListByDate(dateIn);
        if (removeList != null) {
            Order removeOrder = findOrderByNum(orderNum, removeList);
            if (removeOrder != null) {
                removeList.remove(removeOrder);
            } else {
                System.out.println("Your order number is not existed for " + orderNum);
            }
        } else {
            System.out.println("There is currently no order for " + dateIn);
        }

    }

    //probably will get rid of

    @Override
    public String generateOrderNum(String dateIn) {
        int newNum = 0;
        String newOrderNum = "1";
        ArrayList<Order> searchList = findOrderListByDate(dateIn);
        if (searchList != null) {
            for (Order k : searchList) {
                if (Integer.parseInt(k.getOrderNum()) > newNum) {
                    newNum = Integer.parseInt(k.getOrderNum());
                }

            }
            newNum = newNum + 1;
            return newOrderNum = String.valueOf(newNum);
        } else {
            return newOrderNum;
        }

    }
}
