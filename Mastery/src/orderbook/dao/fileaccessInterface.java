/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.dao;

import java.util.ArrayList;
import java.util.HashMap;
import orderbook.operations.Order;

/**
 *
 * @author apprentice
 */
public interface fileaccessInterface {

    public ArrayList<String[]> reader(String fileName)throws Exception;

    public void write(HashMap<String, ArrayList<Order>> mapToPrint)throws Exception;
    
    
}
