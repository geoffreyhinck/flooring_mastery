/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.dao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import orderbook.operations.Order;

/**
 *
 * @author apprentice
 */
public class FileAccess {

    public ArrayList<String[]> reader(String fileName) throws FileNotFoundException {

        Scanner sc = new Scanner(new BufferedReader(new FileReader(fileName)));
        String current;
        ArrayList<String[]> ordersInDate = new ArrayList<String[]>();
        String[] splitLine = null;

        while (sc.hasNextLine()) {
            current = sc.nextLine();
            splitLine = current.split(",");
            ordersInDate.add(splitLine);
        }

        sc.close();

        return ordersInDate;

        //null pointer to handle
    }

    public void write(String fileName, ArrayList<Order> orderList) throws IOException {

        try {
            PrintWriter output = new PrintWriter(new FileWriter(fileName));

            for (Order order : orderList) {
                String orderLine = order.write();
                output.println(orderLine);
            }
            output.println("");
            output.flush();
            output.close();
        } catch (IOException e) {
            System.out.println("Could not write the file for the date entered: ");
        }
    }
}
